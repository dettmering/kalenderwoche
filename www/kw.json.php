<?php

$date = new DateTime();
$week = $date->format("W");

$data = [ 'KW' => $week ];

header('Content-type: application/json');
echo json_encode( $data );
?>
