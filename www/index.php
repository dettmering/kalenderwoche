<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<style>
body	{ 
	font-family: sans-serif;
	background-color: #eaeaea;
        color: #222222;
}
h1	{
	font-size: 100px; 
	text-align: center;
}
@media (prefers-color-scheme: dark) {
body	{
	background-color: #292a2d;
	color: #a9a9b3;
}
}
</style>
<title><?php
$date = new DateTime();
$week = $date->format("W");
echo "KW $week"; ?></title>
</head>
<body>
<h1><?php echo "KW $week"; ?></h1>
</body>
</html>
