FROM php:apache

WORKDIR /var/www/html
COPY ./www/* /var/www/html/

CMD [ "apache2-foreground" ]
